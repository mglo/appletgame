package com.mglo.game.characters;

import com.mglo.game.engine.Background;
import com.mglo.game.engine.StartingClass;

import java.util.ArrayList;

public class Robot {

    //constants
    final int MOVESPEED = 5;
    final int GROUND = 382;

    private int centerX = 100;
    private int centerY = GROUND;
    private boolean jumped = false;
    private boolean movingLeft = false;
    private boolean movingRight = false;
    private boolean ducked = false;

    private static Background bg1 = StartingClass.getBg1();
    private static Background bg2 = StartingClass.getBg2();

    private int speedX = 0;
    private int speedY = 1;

    private ArrayList<Projectile> projectiles = new ArrayList<Projectile>();

    public void update(){

        //Moves Character or scrolls Background accordingly
        if(speedX < 0){ //if left is pressed
            centerX += speedX;
        }
        if(speedX == 0 || speedX < 0){ //if nothing is pressed ( left or right)
            bg1.setSpeedX(0);
            bg2.setSpeedX(0);
        }

        if(centerX <= 400 && speedX > 0){ //when Right is pressed but less than 400
            centerX += speedX;
        }

        //only background setup
        if(speedX > 0 && centerX > 400){
            bg1.setSpeedX(-MOVESPEED);
            bg2.setSpeedX(-MOVESPEED);
        }

        //updates Y position
        centerY += speedY;
        if(centerY + speedY >= GROUND){ //382 is ground level for player
            centerY = GROUND;
        }

        //Handles Jumping
        if(jumped){
            speedY += 1;

            if(centerY + speedY >= GROUND){
                centerY = GROUND;
                speedY = 0;
                jumped = false;
            }
        }

        //prevents going beyond X coordinate of 0 - left side of screen
        if(centerX + speedX <= 60){
            centerX = 61;
        }
    }

    public void moveRight(){
        if(ducked == false){
            speedX = 6;
        }
    }

    public void moveLeft(){
        if(ducked == false){
            speedX = -6;
        }
    }

    public void stopRight(){
        setMovingRight(false);
        stop();
    }

    public void stopLeft() {
        setMovingLeft(false);
        stop();
    }

    public void stop(){
        if(isMovingLeft() == false && isMovingLeft() == false){
            speedX = 0;
        }
        if(isMovingRight() == false && isMovingLeft() == true){
            moveLeft();
        }
        if(isMovingRight() == true && isMovingLeft() == false){
            moveRight();
        }
    }

    public void jump(){
        if(!jumped){
            speedY =-15;
            jumped = true;
        }
    }

    public void shoot(){
        Projectile p = new Projectile(centerX + 50, centerY -25);
        projectiles.add(p);
    }


    public int getCenterX() {
        return centerX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public boolean isJumped() {
        return jumped;
    }

    public void setJumped(boolean jumped) {
        this.jumped = jumped;
    }

    public int getSpeedX() {
        return speedX;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public void setSpeedY(int speedY) {
        this.speedY = speedY;
    }

    public void setMovingLeft(boolean movingLeft) {
        this.movingLeft = movingLeft;
    }

    public void setMovingRight(boolean movingRight) {
        this.movingRight = movingRight;
    }

    public boolean isMovingLeft() {
        return movingLeft;
    }

    public boolean isMovingRight() {
        return movingRight;
    }

    public boolean isDucked() {
        return ducked;
    }

    public void setDucked(boolean ducked) {
        this.ducked = ducked;
    }

    public ArrayList<Projectile> getProjectiles() {
        return projectiles;
    }
}
