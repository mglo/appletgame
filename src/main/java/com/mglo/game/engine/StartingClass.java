package com.mglo.game.engine;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;

import com.mglo.game.characters.Helicopter;
import com.mglo.game.characters.Projectile;
import com.mglo.game.characters.Robot;
import com.mglo.game.framework.Animation;

import javax.imageio.ImageIO;

public class StartingClass extends Applet implements Runnable, KeyListener {

    private Robot robot;
    private Helicopter hb, hb2;
    private Image image, character, character2, character3,
            backgroundIMG, characterDown, characterJumped, currentSprite,
            helicopter, helicop2, helicop3, helicop4, helicop5;
    private Graphics second;
    private URL base;
    private static Background bg1, bg2;
    private Animation anim, heliAnim;

    @Override
    public void init() {
        setSize(800,480);
        setBackground(Color.BLACK);
        setFocusable(true);
        addKeyListener(this);
        Frame frame = (Frame) this.getParent().getParent();
        frame.setTitle("Q-Bot Alpha");

        //Image setup
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream isCharacter = classloader.getResourceAsStream("character.png");
        InputStream isCharacter2 = classloader.getResourceAsStream("character2.png");
        InputStream isCharacter3 = classloader.getResourceAsStream("character3.png");

        InputStream isJumped = classloader.getResourceAsStream("jumped.png");
        InputStream isDown = classloader.getResourceAsStream("down.png");
        InputStream isBackground = classloader.getResourceAsStream("background.png");

        InputStream isHelicopter = classloader.getResourceAsStream("heliboy.png");
        InputStream isHelicopter2 = classloader.getResourceAsStream("heliboy2.png");
        InputStream isHelicopter3 = classloader.getResourceAsStream("heliboy3.png");
        InputStream isHelicopter4 = classloader.getResourceAsStream("heliboy4.png");
        InputStream isHelicopter5 = classloader.getResourceAsStream("heliboy5.png");

        try {
            character = ImageIO.read(isCharacter);
            character2 = ImageIO.read(isCharacter2);
            character3 = ImageIO.read(isCharacter3);

            characterJumped = ImageIO.read(isJumped);
            characterDown = ImageIO.read(isDown);
            backgroundIMG = ImageIO.read(isBackground);

            helicopter = ImageIO.read(isHelicopter);
            helicop2 = ImageIO.read(isHelicopter2);
            helicop3 = ImageIO.read(isHelicopter3);
            helicop4 = ImageIO.read(isHelicopter4);
            helicop5 = ImageIO.read(isHelicopter5);
        } catch (IOException e) {
            e.printStackTrace();
        }

        anim = new Animation();
        anim.addFrame(character, 1250);
        anim.addFrame(character2, 50);
        anim.addFrame(character3, 50);
        anim.addFrame(character2, 50);

        heliAnim = new Animation();
        heliAnim.addFrame(helicopter, 100);
        heliAnim.addFrame(helicop2, 100);
        heliAnim.addFrame(helicop3, 100);
        heliAnim.addFrame(helicop4, 100);
        heliAnim.addFrame(helicop5, 100);
        heliAnim.addFrame(helicop4, 100);
        heliAnim.addFrame(helicop3, 100);
        heliAnim.addFrame(helicop2, 100);

        currentSprite = anim.getImage();

    }

    @Override
    public void start() {
        bg1 = new Background(0,0);
        bg2 = new Background(2160, 0);

        hb = new Helicopter(340, 360);
        hb2 = new Helicopter(700, 360);
            robot = new Robot();

            Thread thread = new Thread(this);
            thread.start();
    }

    @Override
    public void stop() {
    }

    @Override
    public void destroy() {
    }

    public void run() {
        while(true){
            robot.update();
            if(robot.isJumped()){
                currentSprite = characterJumped;
            }else if(robot.isJumped() == false && robot.isDucked() == false){
                currentSprite = anim.getImage();
            }

            //only for moving bullets
            ArrayList projectiles = robot.getProjectiles();
            for (int i = 0; i < projectiles.size(); i++) {
                Projectile p = (Projectile) projectiles.get(i);
                if(p.isVisible() == true){
                    p.update();
                }else{
                    projectiles.remove(i);
                }
            }//END//

            hb.update();
            hb2.update();
            bg1.update();
            bg2.update();

            animate();
            repaint();
            try {
                Thread.sleep(17);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void animate(){
        anim.update(10);
        heliAnim.update(50);
    }

    @Override
    public void update(Graphics g) {
        if(image == null){
            image = createImage(this.getWidth(), this.getHeight());
            second = image.getGraphics();
        }
        second.setColor(getBackground());
        second.fillRect(0,0,getWidth(),getHeight());
        second.setColor(getForeground());
        paint(second);

        g.drawImage(image, 0, 0,this);
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(backgroundIMG, bg1.getBgX(), bg1.getBgY(), this);
        g.drawImage(backgroundIMG, bg2.getBgX(), bg2.getBgY(), this);

        ArrayList projectiles = robot.getProjectiles();
        for (int i = 0; i < projectiles.size(); i++) {
            Projectile p = (Projectile) projectiles.get(i);
            g.setColor(Color.YELLOW);
            g.fillRect(p.getX(), p.getY(), 10, 5);
        }


        g.drawImage(currentSprite, robot.getCenterX()-61, robot.getCenterY()-63, this);
        g.drawImage(heliAnim.getImage(), hb.getCenterX()-48, hb.getCenterY()-48, this);
        g.drawImage(heliAnim.getImage(), hb2.getCenterX()-48, hb2.getCenterY()-48, this);
    }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()){
            case KeyEvent.VK_UP:
                System.out.println("Jumping");
                robot.jump();
                break;
            case KeyEvent.VK_DOWN:
                currentSprite = characterDown;
                if(robot.isJumped() == false){
                    robot.setDucked(true);
                    robot.setSpeedX(0);
                }
                break;
            case KeyEvent.VK_LEFT:
                System.out.println("Move Left");
                robot.moveLeft();
                robot.setMovingLeft(true);
                break;
            case KeyEvent.VK_RIGHT:
                System.out.println("Move Right");
                robot.moveRight();
                robot.setMovingRight(true);
                break;
            case KeyEvent.VK_SPACE:
                System.out.println("Shooting");
                if(robot.isDucked() == false && robot.isJumped() == false){
                    robot.shoot();
                }
                break;
        }
    }

    public void keyReleased(KeyEvent e) {
        switch(e.getKeyCode()){
            case KeyEvent.VK_UP:
                System.out.println("Stop jumping");
                break;
            case KeyEvent.VK_DOWN:
                System.out.println("Stop moving Down");
                currentSprite = character;
                robot.setDucked(false);
                break;
            case KeyEvent.VK_LEFT:
                System.out.println("Stop moving Left");
                robot.stopLeft();
                break;
            case KeyEvent.VK_RIGHT:
                System.out.println("Stop moving Right");
                robot.stopRight();
                break;
        }
    }

    public static Background getBg1() {
        return bg1;
    }

    public static Background getBg2() {
        return bg2;
    }

}
